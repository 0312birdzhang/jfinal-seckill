package com.ljph.seckill.exception;

/**
 * 秒杀关闭异常
 * Created by yuzhou on 16/8/27.
 */
public class SeckillCloseException extends SeckillException {

    public SeckillCloseException(String message) {
        super(message);
    }

    public SeckillCloseException(String message, Throwable cause) {
        super(message, cause);
    }
}
